import i18n from 'i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(reactI18nextModule)
  .init({
    fallbackLng: 'en',

    // have a common namespace used around the full app
    ns: ['common'], // array for configuring namespaces
    defaultNS: 'common',

    debug: process.env.NODE_ENV === 'development',

    interpolation: {
      escapeValue: false // not needed for react!!
    },

    react: {
      wait: true // // use wait option to not render before loaded
    },

    nonExplicitWhitelist: true,

    // accessing an object not a translation string
    returnObjects: true,

    backend: {
      // for all available options read the backend's repository readme file
      loadPath: `http://localhost:1337/locales/{{lng}}/{{ns}}`
    }
  });

export default i18n;
