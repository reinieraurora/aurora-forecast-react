// @flow
import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1
  }
};

type Props = {
  classes: Object
};

class AppBar extends React.PureComponent<Props> {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <MuiAppBar position="static" color="default">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Aurora forecast
            </Typography>
          </Toolbar>
        </MuiAppBar>
      </div>
    );
  }
}

export default withStyles(styles)(AppBar);
