import React from 'react';
import { Route, Link } from 'react-router-dom';
import Home from 'containers/_DEV/home';
import About from 'containers/_DEV/about';

const DevBar = () =>
  process.env.NODE_ENV === 'development' ? (
    <div>
      <header style={{ width: '100%', backgroundColor: '#666' }}>
        DEVBAR
        <Link href="/dev" to="/dev">
          Home
        </Link>
        <Link href="/dev/about-us" to="/dev/about-us">
          About
        </Link>
      </header>
      <main>
        <Route exact path="/dev" component={Home} />
        <Route exact path="/dev/about-us" component={About} />
      </main>
    </div>
  ) : null;

export default DevBar;
