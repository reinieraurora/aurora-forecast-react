// @flow

import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Text from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

type Props = {
  classes: Object
};

const styles = {
  card: {
    display: 'flex'
  },
  nowDetails: {},
  upcomingDetails: {}
};

const KpWidget = ({ classes }: Props) => (
  <Card className={classes.card}>
    <CardContent>
      <Text variant="title">Chance to see northern lights</Text>
      <div className={classes.nowDetails}>
        <Text>now</Text>
        <Text>80%</Text>
      </div>
      <div className={classes.upcomingDetails}>
        <Text>15 mins</Text>
        <Text>30 mins</Text>
        <Text>1 hour</Text>
      </div>
    </CardContent>
  </Card>
);

KpWidget.defaultProps = {};

export default withStyles(styles)(KpWidget);
