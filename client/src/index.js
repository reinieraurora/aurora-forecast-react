import DevBar from 'components/_DEV/DevBar';
import App from 'containers/App';
import Theme from 'containers/App/Theme';
import { I18nextProvider } from 'react-i18next';
import i18n from 'lib/i18n';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';
import registerServiceWorker from './registerServiceWorker';

const target = document.querySelector('#root');

render(
  <Theme>
    <Provider store={store}>
      <I18nextProvider i18n={i18n}>
        <ConnectedRouter history={history}>
          <div>
            <DevBar />
            <App />
          </div>
        </ConnectedRouter>
      </I18nextProvider>
    </Provider>
  </Theme>,
  target
);

registerServiceWorker();
