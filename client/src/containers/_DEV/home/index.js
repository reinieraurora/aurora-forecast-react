// @flow

import { actions } from 'modules/_Dev/counter';
import React from 'react';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

type Props = {
  count: number,
  increment: Function,
  isIncrementing: boolean,
  isDecrementing: boolean,
  changePage: Function,
  incrementAsync: Function,
  decrement: Function,
  decrementAsync: Function
};

// eslint-disable-next-line react/prefer-stateless-function
class Home extends React.PureComponent<Props> {
  render() {
    const {
      count,
      increment,
      isIncrementing,
      isDecrementing,
      changePage,
      incrementAsync,
      decrement,
      decrementAsync
    } = this.props;

    return (
      <div>
        <h1>Home</h1>
        <p>Count: {count}</p>

        <p>
          <button onClick={increment} disabled={isIncrementing}>
            Increment
          </button>
          <button onClick={incrementAsync} disabled={isIncrementing}>
            Increment Async
          </button>
        </p>

        <p>
          <button onClick={decrement} disabled={isDecrementing}>
            Decrement
          </button>
          <button onClick={decrementAsync} disabled={isDecrementing}>
            Decrement Async
          </button>
        </p>

        <p>
          <button onClick={() => changePage()}>
            Go to about page via redux
          </button>
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  count: state.counter.count,
  isIncrementing: state.counter.isIncrementing,
  isDecrementing: state.counter.isDecrementing
});

const mapDispatchToProps = {
  ...actions,
  changePage: () => push('/dev/about-us')
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
