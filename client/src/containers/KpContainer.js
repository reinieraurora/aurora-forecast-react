// @flow
import * as React from 'react';
import { connect } from 'react-redux';

type Props = {
  children: React.Node,
  kpData: Object
};

class KpContainer extends React.PureComponent<Props> {
  render() {
    const { children, kpData } = this.props;

    return React.Children.map(children, c =>
      React.cloneElement(c, {
        kpData
      })
    );
  }
}

const mapStateToProps = state => ({
  kpData: state.kpData
});

export default connect(mapStateToProps)(KpContainer);
