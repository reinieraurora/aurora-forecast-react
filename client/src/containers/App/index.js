import React from 'react';
import { Route } from 'react-router-dom';
import AppBar from 'components/AppBar';
import LayoutNowForecast from 'layouts/forecasts/now';

const App = () => (
  <React.Fragment>
    <nav>
      <AppBar />
    </nav>
    <main>
      <Route exact path="/" component={LayoutNowForecast} />
    </main>
  </React.Fragment>
);

export default App;
