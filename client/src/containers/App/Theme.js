// @flow
import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import 'typeface-roboto/index.css';

const theme = createMuiTheme({
  palette: {
    type: 'dark'
  }
});

type Props = {
  children: Object
};

const Theme = (props: Props) => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    {props.children}
  </MuiThemeProvider>
);

export default Theme;
