import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import counter from 'modules/_Dev/counter';

const reducers = {
  router: routerReducer
};

if (process.env.NODE_ENV === 'development') {
  reducers.counter = counter;
}

export default combineReducers(reducers);
