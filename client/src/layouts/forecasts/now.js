// @flow

import * as React from 'react';
import KpContainer from 'containers/KpContainer';
import KpWidget from 'components/KpWidget';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

type Props = {
  classes: Object
};

const styles = theme => ({
  container: {
    padding: theme.spacing.unit * 2
  }
});

// eslint-disable-next-line react/prefer-stateless-function
class NowForecastLayout extends React.PureComponent<Props> {
  render() {
    const { classes } = this.props;

    return (
      <Grid container className={classes.container}>
        <Grid item xs={4}>
          <KpContainer>
            <KpWidget />
          </KpContainer>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(NowForecastLayout);
