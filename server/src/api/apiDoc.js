const apiDoc = {
  swagger: '2.0',
  basePath: '/',
  info: {
    title: 'API',
    version: '1.0.0'
  },
  definitions: {
    Developer: {
      type: 'object',
      properties: {
        id: {
          description: 'The id of this developer.',
          type: 'string'
        },
        firstName: {
          description: 'The id of this developer.',
          type: 'string'
        },
        lastName: {
          description: 'The id of this developer.',
          type: 'string'
        },
        expertise: {
          description: 'The id of this developer.',
          type: 'string'
        },
        os: {
          description: 'The language of this translation.',
          type: 'string'
        }
      },
      required: ['firstName']
    }
  },
  // Our paths object was empty in the main apiDoc because express-openapi will generate it for us
  // based on the location of our path handlers. For this example we'll place our path handlers
  paths: {}
};

export default apiDoc;
