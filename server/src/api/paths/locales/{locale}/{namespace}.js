module.exports = localesService => {
  const GET = (req, res) => {
    res
      .status(200)
      .json(
        localesService.getTranslations(req.params.locale, req.params.namespace)
      );
  };

  GET.apiDoc = {
    summary: 'Returns translations by locale and namespace.',
    tags: ['Locales'],
    operationId: 'getTranslations',
    parameters: [
      {
        in: 'path',
        name: 'locale',
        required: true,
        type: 'string'
      },
      {
        in: 'path',
        name: 'namespace',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      200: {
        description:
          'An object of translations that match the requested language and namespace.'
      },
      default: {
        description: 'An error occurred',
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  return {
    GET
  };
};
