module.exports = developersService => {
  const GET = async (req, res) => {
    res.status(200).json(await developersService.all());
  };

  const POST = async (req, res) => {
    res.status(201).json(await developersService.save(null, req.body));
  };

  GET.apiDoc = {
    summary: 'Returns all developers.',
    tags: ['DEV'],
    operationId: 'all',
    responses: {
      200: {
        description: 'A list of developers.',
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/Developer'
          }
        }
      },
      default: {
        description: 'An error occurred',

        schema: {
          additionalProperties: true
        }
      }
    }
  };

  POST.apiDoc = {
    summary: 'Creates a new developer',
    tags: ['DEV'],
    operationId: 'create',
    parameters: [
      {
        in: 'body',
        name: 'body',
        description: 'Developer object that needs to be added',
        required: true,
        schema: {
          $ref: '#/definitions/Developer'
        }
      }
    ],
    responses: {
      201: {
        description: 'The developer was created successfully.'
      },
      default: {
        description: 'An error occurred',
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  return {
    GET,
    POST
  };
};
