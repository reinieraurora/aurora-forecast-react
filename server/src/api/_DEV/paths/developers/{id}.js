module.exports = developersService => {
  const GET = async (req, res) => {
    res.status(200).json(await developersService.getById(req.params.id));
  };

  const PUT = async (req, res) => {
    res.status(204).json(await developersService.save(req.params.id, req.body));
  };

  const DELETE = (req, res) => {
    res.status(204).json(developersService.remove(req.params.id));
  };

  GET.apiDoc = {
    summary: 'Returns a developer by id.',
    tags: ['DEV'],
    operationId: 'getById',
    parameters: [
      {
        in: 'path',
        name: 'id',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      200: {
        description: 'A developer that matches the requested id.',
        schema: {
          $ref: '#/definitions/Developer'
        }
      },
      default: {
        description: 'An error occurred',
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  PUT.apiDoc = {
    summary: 'Updates a developer by id.',
    tags: ['DEV'],
    operationId: 'update',
    parameters: [
      {
        in: 'path',
        name: 'id',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'body',
        description: 'Developer object that needs to be updated',
        required: true,
        schema: {
          $ref: '#/definitions/Developer'
        }
      }
    ],
    responses: {
      204: {
        description: 'The developer was updated successfully.'
      },
      default: {
        description: 'An error occurred',
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  DELETE.apiDoc = {
    summary: 'Removes a developer by id.',
    tags: ['DEV'],
    operationId: 'remove',
    parameters: [
      {
        in: 'path',
        name: 'id',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      204: {
        description: 'The developer was deleted successfully.'
      },
      default: {
        description: 'An error occurred',
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  return {
    GET,
    PUT,
    DELETE
  };
};
