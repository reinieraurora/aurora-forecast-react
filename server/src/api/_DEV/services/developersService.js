// @flow
import fs from 'fs';
import uid from 'uniqid';

type TDeveloper = {
  id: string,
  firstName: string,
  lastName?: string,
  expertise?: string,
  os?: string
};

export default {
  /**
   * Return a list of developers
   * @returns {Array}
   */
  async all(): ?Promise<Array<?TDeveloper>> {
    const developers = await import('../mock-data/developers.json');

    return developers || [];
  },

  /**
   * Gets a developer by id
   * @param id
   * @returns {TDeveloper | undefined}
   */
  async getById(id: string): ?Promise<TDeveloper> {
    const developers = await import('../mock-data/developers.json');

    return developers.find(d => d.id === id);
  },

  /**
   * Creates or updates a developer
   * @param id
   * @param developerData
   */
  async save(id: string, developerData: TDeveloper) {
    const developers = await import('../mock-data/developers.json');

    if (!id) {
      developerData.id = uid();
      developers.push(developerData);
    } else {
      const index = developers.findIndex(d => d.id === id);
      const developer = developers.find(d => d.id === id);

      delete developerData.id;

      developers[index] = { ...developer, ...developerData };
    }

    return fs.writeFile(
      '../mock-data/developers.json',
      JSON.stringify(developers, null, 4)
    );
  },

  /**
   * Removes a developer
   * @param id
   */
  async remove(id: string) {
    const developers = await import('../mock-data/developers.json');
    const index = developers.findIndex(d => d.id === id);

    developers.splice(index, 1);

    return fs.writeFile(
      '../mock-data/developers.json',
      JSON.stringify(developers, null, 4)
    );
  }
};
