// @flow

import fs from 'fs';
import path from 'path';

const localesService = {
  getTranslations(locale: string, namespace: string) {
    const translations = JSON.parse(
      fs.readFileSync(path.resolve(`src/locales/${namespace}.json`), 'utf8')
    );

    const [language] = locale.includes('-') ? locale.split('-') : [locale];

    return translations[language];
  }
};

export default localesService;
