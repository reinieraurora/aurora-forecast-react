import express from 'express';
import bodyParser from 'body-parser';
import openapi from 'express-openapi';
import cors from 'cors';
import winston from 'winston';
import apiDoc from 'api/apiDoc';
import logger from 'lib/logger';

/* IMPORT SERVICES */
import developersService from 'api/_DEV/services/developersService';
import localesService from 'api/services/localesService';

/* SETUP APP */
const hostname = process.env.NODE_HOST || '127.0.0.1';
const port = process.env.NODE_PORT || 1337;
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.disable('x-powered-by');

/* SETUP OPENAPI */
const dependencies = { localesService };
const paths = ['src/api/paths'];

if (process.env.NODE_ENV === 'development') {
  dependencies.developersService = developersService;
  paths.push('src/api/_DEV/paths');
} else {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple()
    })
  );
}

openapi.initialize({
  apiDoc,
  app,
  dependencies,
  paths
});

/* START SERVER */
app.listen(port, () => {
  logger.info(`Server running at http://${hostname}:${port}/`);
});
